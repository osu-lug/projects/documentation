# Git
Software that keeps track of changes to files to do version control as well as help with multiple concurrent editors.
## Basic Overview
<https://rogerdudler.github.io/git-guide/>
## Github's Guide
Explains more in-depth about each command, useful for beginners wanting to learn more.

<https://github.com/git-guides>
## Full Reference
<https://git-scm.com/docs>
## Git Workflows
Ultimately there are many git flows out there, each with their pros and cons, use what is best for your project. Some flows are more complex, but ensure organization, while others keep it simple, but are less strict on keeping it organized.
### Example Lightweight Github flow
<https://guides.github.com/introduction/flow/>
### Example Remote Github Workflow
<https://guides.github.com/introduction/git-handbook/#github>
### Another Example Workflow
<https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows>
### Another Hugely Popular Git Flow
<https://nvie.com/posts/a-successful-git-branching-model/>