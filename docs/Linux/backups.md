# Backups
<https://www.redhat.com/sysadmin/5-backup-tips>
## syncthing
File synchronization with options for file history. Easy to setup.

<https://syncthing.net/>
## duplicati
Backups that can connect and push to many different sources

<https://www.duplicati.com/>
## DejaDup
Simple GNOME backup tool.

<https://wiki.gnome.org/Apps/DejaDup>

<https://www.howtoforge.com/tutorial/ubuntu-backup-deja-dup/>
## rsync
Raw file sync between two directories.

<https://rsync.samba.org/>