# Package Managers

A Package manager keeps track of what "packages" are installed on your system. These packages contain the software and the libraries that are needed to run that software.

### Graphical Front Ends by Distro

-   Ubuntu: Ubuntu Software

-   Debian: synaptic (not built-in)

-   Arch: pamac-manager (built-in with Manjaro)

### List by Distro

-   Debian: apt

-   Arch: pacman

-   Fedora/CentOS: dnf/yum

-   OpenSUSE: zypper

-   Gentoo: portage

### Using apt

Where `NAME` is a package name or description (or part of one) and `PACKAGE` is a package name

-   Search: `sudo apt search PACKAGE`

-   Install: `sudo apt install PACKAGE`

-   Update: `sudo apt update` then `sudo apt upgrade`

### Using pacman

Where `NAME` is a package name or description (or part of one) and `PACKAGE` is a package name

-   Search: `sudo pacman -Q NAME`

-   Install: `sudo pacman -Sy NAME`

-   Update: `sudo pacman -Syu update`

### Using dnf or yum

Where `NAME` is a package name or description (or part of one) and `PACKAGE` is a package name

-   Search: `sudo dnf search PACKAGE`

-   Install: `sudo dnf install PACKAGE`

-   Update: `sudo dnf upgrade`
