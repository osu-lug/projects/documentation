# Desktop Environment
This defines how the windows are arranged, and other workflow related tasks (like the home menu). Also called a Window Manager.

## Choosing a Desktop Environment

### Beginner

#### GNOME

-   Defaults on Fedora, Debian, Ubuntu, CentOS, etc.

-   One of the larger contributor networks

-   Many familiar features to MacOS and Windows

![image](https://www.gnome.org/wp-content/uploads/2017/04/featured-image@2x-1.png)

#### XFCE

-   Lightweight and performant

-   "Aims to be fast and low on system resources"

![image](https://cdn.xfce.org/about/screenshots/4.14-1.png)

#### Cinnamon

-   Fork of GNOME 3, for Linux Mint

-   Created after disagreement of GNOME 3 design direction, follows more traditional patterns

![image](https://upload.wikimedia.org/wikipedia/commons/c/cd/Cinnamon_4.2.3_screenshot.png)

#### KDE Plasma

-   Most Windows-like

-   Multiple virtual desktops, with custom layouts for each

-   Also a very large contributor and support network

![image](https://kde.org/announcements/plasma-5.17/plasma-5.17.png)

#### MATE

-   The continuation of GNOME 2

-   Interested in "preserving a traditional desktop experience"

![image](https://mate-desktop.org/gallery/1.22/english/Mate-1.22-3.png)

### Intermediate

#### i3

-   vi-like control system, mostly with no mouse control

-   New windows maximize screen size by default, in a tiling-manner.

-   Minimal interface and features, just enough to provide access to program-specific GUIs

This is tiling window manager (windows split the screen as shown) ![image](images/i3){width="0.7\linewidth"} Source: [i3 website](https://i3wm.org/screenshots/i3-9.png)

#### bspwm

-   Tiling window manager like i3, but represents windows as leaves of a full binary tree.

-   Focuses on configuration and standardization through X11 events, and uses separate program (bspc) to manage communication to the window manager itself, which handles all keyboard and mouse input, process signals, etc.

#### LXDE

-   Performance as a priority

-   Highly modular, with few inner dependencies making it easier to install only what a users wants or needs

![image](https://commons.wikimedia.org/wiki/File:LXDE_desktop_full.png)

### Advanced

These are highly configurable, but they don't work well out of the box

#### Openbox

-   Stacking window manager, meaning windows drawn in specific order, allowing windows to display on top of one another.

-   Programs run in boxes, and openbox allows for full control on howboxes interact, run, display, etc.

![image](https://upload.wikimedia.org/wikipedia/commons/b/b0/Openbox-elementary3.png)

#### Awesome

-   Aims to provide a "framework window manager", where there is very minimal features and you must configure it to get useability.

-   Provides standardized API to interact with, with out-of-the-box scripting available using the Lua programming language.

![image](https://awesomewm.org/images/screen.png)