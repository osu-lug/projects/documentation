# Folder Layout

### bin

For Binary files

### boot

Basic files to start the boot process

### dev

A "virtual" file system

#### /dev/urandom
Outputs a stream of random numbers
#### /dev/nvme
Nvme hard drives can be found starting with this path. Normally `/dev/nvme${DRIVEINDEX}p${PARTITION_NUMBER}` (for example `/dev/nvme0p0`)
#### /dev/sd
Normal hard drives are found here, `/dev/sd${LETTER}$(NUMBER)`, where letter is the drive letter, and number is the partition number (for example `/dev/sda1`).
#### /dev/zero
### etc

Additional libraries and configuration files needed to complete the boot process

### home

Where your files are stored, and your configurations

### lib

Library files. Needed for most binaries.

### lib64

Library files for 64bit systems. Needed for most binaries.

### lost+found

Files that for some reason got partially corrupted go here. Almost always empty (and you want it that way)

### media

A place to mount USBs/other external media automatically

### mnt

A place to mount external media manually

### opt

A place to install proprietary and other software that is self-contained

### proc

A "virtual" file system that has process information

### root

A home for the "root" user

### run

General information about the system that is valid until reboot.

### sbin

Super User (root) binaries. These binaries normally modify your system in some way, and are not needed for normal use.

### sys

A "virtual" file system that represents your physical hardware.

### tmp

A place for temporary files that do not persist across reboots.

### usr

Additional software, normally managed by your package manager. Some configuration files are here as well.

### usr/local

A place for you to add additional software, outside of the package manager. Not normally needed.

### var

Varies state about the system that persists across reboots