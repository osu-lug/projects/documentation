# Docker
<https://docs.docker.com/get-started/>
## Docker Hub
A repository for Docker images. Can also create your own.

<https://hub.docker.com/>
## Dockerfile
<https://docs.docker.com/develop/develop-images/dockerfile_best-practices/>

<https://docs.docker.com/engine/reference/builder/>
## Docker Compose
Simple YAML for orchestrating groups of containers together
<https://docs.docker.com/compose/>

<https://docs.docker.com/compose/compose-file/>