# Rescue
If you are here, something has gone wrong.
## My System Won't Boot

### Windows

NOTE: This section only applies to problems arising when dual-booting. If you choose Windows in Grub, and it doesn't boot, follow these steps.

1.  Boot into a Windows Live Media (also called Recovery Media or
    Installation Media)

2.  Hit next

3.  Choose "repair your computer"

4.  Hit Troubleshoot

5.  Hit Command Prompt

6.  Execute `bootrec /fixmbr`

7.  Execute `bcdboot c:/windows`

8.  Try booting now

### Linux

1.  Boot into a Live USB

2.  Find your root partition with gparted

    1.  Launch gparted

    2.  Note the hard drive name (`DRIVE`) and the name of your main partition (`ROOT-PATH`) (something like /dev/sda1 or /dev/nvme0np1)

3.  To re-install grub run these commands

    1.  `sudo mount ROOT-PATH /mnt`

    2.  `sudo mount –bind /sys /mnt/sys`

    3.  `sudo mount –bind /proc /mnt/proc`

    4.  `sudo mount –bind /dev /mnt/dev`

    5.  If you have a separate boot partition, mount that now to /mnt/boot

    6.  `sudo chroot /mnt`

    7.  `sudo grub-install DRIVE`

    8.  `sudo grub-mkconfig -o /boot/grub/grub.cfg` (make sure /boot/grub exists, it may be under /boot/grub2)


Booting a live iso and re-installing grub