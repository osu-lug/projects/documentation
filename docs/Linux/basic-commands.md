# Basic Commands
## Overview
Generally speaking you execute a command with some arguments and flags.
### Argument
An argument is just some string given to the binary. For example in the command to change to your Documents directory `cd Documents`, `Documents` is the argument. 
### Flags (or OPTIONS)
Flags are used to modify the behavior of a command. For example `ls -a`, `-a` tells the ls executable to list all files in the directoy, including hidden ones. Flags are also called OPTIONS.
## Commands
### cd 
Change Directory, changes your "working directory" to the specified folder. For example `cd Documents` changes to your documents folder
### pwd
Print Working Directory. Outputs your current working directory to console. Something like `/home/user/Documents/`.
### ls
LiSt files. Lists the files in your current directory. Use the `-a` flag to see hidden folders.
### echo
Echos the string given as an argument.
## More resources
<https://www.guru99.com/must-know-linux-commands.html>

<https://www.redhat.com/sysadmin/basic-linux-commands>

<https://www.hostinger.in/tutorials/linux-commands>
## Under The Hood
### Command
A command is really just an executable found in your `$PATH`. PATH is the envoirment variable that holds where to look for executables. You can see the value of this by executing `echo $PATH`.
### Envoirment Variables
Envoirment Variables are variables used to pass into executables. PATH is an envoirmental variable, so is HOME. You can see them all with `env`, and set them with `export NAME=VALUE`.
### Getting Home
`~` in most shells is replaced with your home directory (normally /home/USERNAME). You can get the absolute path of your home directory with `echo $HOME`, or just go home with a quick `cd` (no arguments needed).
### 