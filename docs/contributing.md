# Contributing

## Documentation

1. Fork [Our Gitlab repo](https://gitlab.com/osu-lug/projects/documentation)
2. Install dependencies with [`pipenv`](https://github.com/pypa/pipenv)
3. Make your changes
4. Test the site locally: `mkdocs build`
5. Make a Merge Request (See [Git](Tools/git.md) for help using Git). Try to get someone (ideally an OSU LUG officer / maintainer) to review your Merge Request so it can get merged
   - NOTE: "Merge Request" / "MR" is the same as "Pull Request" / "PR" on GitHub
6. After your Merge Request is merged, an OSU LUG officer / maintainer will need to deploy your changes to the [documentation website](https://docs.lug.oregonstate.edu/) (see "Maintainer Instructions" section below for reference)

## Maintainer Instructions

If you are an OSU LUG officer, follow these steps to actually deploy changes (from merged Merge Request's etc) to our [documentation website](https://docs.lug.oregonstate.edu/):

1. Talk to the current President or Infrastructure officer about getting your SSH public key added to the OSU LUG server's list of authorized keys
   - Ask for help from another officer if you need help with setting up your SSH key config (on your own computer) as well.
2. After you have gotten SSH access set up (see [engr-servers](OSU/engr-servers.md) for the basics), run the following commands from your terminal:
   - `ssh osulug`
   - `ssh webserver`
   - `./update-sites.sh`
   - If the commands worked, you should get a similar output to what's shown below:
   - ![example-update-script-execution](https://i.ibb.co/fQJc4Tc/example-update-script-run.png)

## Markdown

See [here](./Linux/programming-langs.md#markdown) for reference.

### Markdown Standards

Although we are using mkdocs, and that does support markdown extensions, only
use standard markdown, so these documents can be portable.

### Including

- Headers
- Lists (both numbered and unnumbered)
- Fenced Code blocks (triple backtick)
- Command (single backtick)
- Images
- Links
- Emphasis (Bold, Italics, Underline, Strike-Through)

### Excluding

- Horizontal Rules (use headers instead) (if you do find a reason to use these, use `***` and specify in the Merge Request why it
  was needed)
- Tables (not standardized) (if you find a case to need these, feel free to do so in the Merge Request)
- Other fancy elements

### Spacing Standards

- Do not enforce a max width.
- Space after the \# in a header before the text.

### Table Of Contents

mkdocs only generates a Table of contents if you have one top level header.
