# Images
## Pixel Based
### GIMP
<https://www.gimp.org/>
## Vector Based
### Inkscape
<https://inkscape.org/>
## Other
### ImageMagik
Can convert files between formats, merge two photos together with transparency, and other basic operations on the command line.

<http://www.imagemagick.org/>
