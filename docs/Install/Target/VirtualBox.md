# Virtualbox

### Initial setup

1.  Install Virtualbox

2.  Enabling Virtualization

    1.  Boot into the BIOS (see [here](./bare-metal.md#entering-the-bios))

    2.  Look for item(s) that say virtualization, vt-d, vt-x or amd-v

    3.  Make sure they are on

3.  Reboot

4.  Go to the website of the distro you want to install, and install the iso.

5.  Open Virtualbox

### Creating the VM

1.  Hit "Machine" in the menu bar

2.  Hit "New"

3.  Choose a name for you VM

4.  Make sure the Type is Linux (if it wasn't change version to generic linux)

5.  Choose the amount of RAM given to your VM (more is better, but make sure you still have enough to run your normal operating system)

6.  Create a virtual disk with 12-16GB if you can afford it

7.  Right click on the name of your VM on the left and Hit settings

8.  Go to the Storage tab

9.  Hit the drive (should be under "Controller IDE")

10. Hit the disk icon to the right of "Optical Drive" on the right pane

11. Browse to where you downloaded the iso, then hit OK

12. See [here](./../installation.md) to install.