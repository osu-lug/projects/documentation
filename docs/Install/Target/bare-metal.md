# Bare Metal
For installing directly on hardware.
## Entering the BIOS
For more information about BIOS see [here](../../Linux/Components/bios.md)


***BE CAREFUL IN THE BIOS.***.


1.  Turn off your computer
2.  Plug in your Installation USB (for more information about bootable usb see [here](../bootable-usb.md))
3.  Turn on the computer
4.  Before the laptop manufacturers logo appears press F2 repeatedly. You should see something like one of the images below, if not see [here](../notes.md#alternate-bios-keys).

![image](https://upload.wikimedia.org/wikipedia/commons/3/3d/Puredyne-bios-01.jpg)

![image](https://upload.wikimedia.org/wikipedia/commons/7/71/Bios-cambiar-ide-ahci.png)

### Allowing the USB to Boot

Unfortunately due to the vast array of layouts of BIOS, all I can do is describe what you are looking for. While you are looking, make sure not to change any settings.

1.  First we need to turn off Secure Boot
    -   Sometimes this setting is alongside a booting option
    -   Sometimes this is under a security tab
    -   Secure Boot will always be labeled "Secure Boot" exactly
2.  Move the USB to the top of the boot order sequence
    -   The boot order sequence is normally under a section on booting
    -   See the bottom of your screen for which keys move items up and
        down in the list, normally it is F5 for Up, F6 for down
3.  Exit the BIOS
    -   Normally this is F10
    -   Make sure you save and exit


## Dual Boot

If you want to dual-boot with existing data on your disk continue following the instructions, otherwise move on.
### Windows - Free Disk Space

Before we can install Linux we have to make some space on your hard drive.

1.  Boot into Windows (if you are unable to do this see Below)

2.  Login

3.  Open Disk Management

4.  Click on C: in the top pane

5.  Right click on the highlighted partition in the bottom pane and choose shrink volume

6.  In "Enter the amount of space to shrink in MB" put the number of MB that you want to give to Linux there.

    -   See [here](../notes.md#how-much-space-does-linux-need) if you are unsure how much to give.

    -   Note that this is in MB. If you want to give it 10 GB, that would be 10\*1024=10240MB.

7.  Hit Shrink and wait.

### Linux - Free Disk Space

1.  Boot into a Live USB (see [here](#entering-the-bios) if need to configure your BIOS) (see [here](../bootable-usb.md) for more information about bootable USBs)

2.  Start Live environment

3.  Start gparted ([install](../post-install.md#installing-software) it if it is not installed)

4.  If you don't see your hard drive see [here](../notes.md#hard-drive-not-detected)

5.  Right click on the largest partition and hit "Resize/Move"

6.  Enter "Free space following" that you want to give to Linux

    -   See [here](../notes.md#how-much-space-does-linux-need) if you are unsure how much to give.

    -   Note that this is in MB. If you want to give it 10 GB, that would be 10\*1024=10240MB.

7.  Hit "Resize/Move"

8.  Ensure that any existing EFI partition is at least 512MB (see [here](../notes.md#linux-resizing-efi))

9.  Hit the checkmark near the top, then hit Apply

10. If the operations fails, ask for help.