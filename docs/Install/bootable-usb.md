# Bootable USB

A Bootable USB is a USB drive that has a mini operating system on it. Just enough of one to allow you to boot.

## Creating

1.  Download an iso to ISO-PATH. An iso is the raw 1s and 0s of the entire operating system: you can find it by searching the internet for "\<distro\> download".

2.  Flash the iso to a USB drive. NOTE: etcher is easier than the below methods, but it sends tracking requests to various sites including google-analytics ([source)](https://github.com/balena-io/etcher/issues/2057#issuecomment-442730925).

    -   If on Windows use a program called "rufus"

    -   If on Mac or Linux

        1.  Open Terminal as administrator

        2.  Find the drive path of the drive you wish to write (`sudo fdisk -l` can help with this)

        3.  MAKE SURE YOU HAVE THE CORRECT DRIVE

        4.  MAKE SURE YOU HAVE THE CORRECT DRIVE AGAIN

        5.  Execute `sudo dd if=ISO-PATH of=DRIVE-PATH status=progress`

### Terminology Notes:

#### Installation Media

A way to install another operating system, sometimes a Live USB.

#### Live USB

A bootable operating system that you can preform normal tasks in. This does not store the files you create, much like a guest session. Can be used for recovery and trying out distros.
