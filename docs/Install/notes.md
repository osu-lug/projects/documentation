# Notes

## How Much Space Does Linux Need?

Linux needs at least 16GB to be usable, although it is recommended to use 32 or 64 if you have enough space. If you plan on having large files in Linux, increase this number to 128. You are always welcome to increase this if you wish.

## Linux - Resizing EFI

1.  Move each partition 512MB left after the EFI partition going right to left (by putting 512 in the space preceding)

2.  Hit the checkmark near the top, then hit Apply

3.  Note the name of the EFI partition (`EFI-PARTITION`) (something like /dev/sda1 or /dev/nvme0np1)

4.  Backup the files in the EFI partition by executing `sudo mount EFI-PARTITION /mnt;mkdir efi;sudo cp -R /mnt/* efi/;sudo umount /mnt/` in a terminal

5.  Delete the EFI partition

6.  Create a new fat32 partition of 612MB where the old EFI was with a label of "EFI"

7.  Hit the checkmark near the top, then hit Apply

8.  Right click on the new EFI partition and "Manage Flags" then add the "esp" and "boot" flags.

9.  Copy the files back by executing `sudo mount EFI-PARTITION /mnt;sudo cp -R efi/* /mnt/;sudo umount /mnt` in a terminal

## Linux - Partitioning

The interface varies per distro, so I will describe what you are looking for. **BE CAREFUL ON THIS STEP**

1.  Create an EFI partition if you don't already have one

    1.  Find the + or the add partition button

    2.  Select a size of 512MB

    3.  Select partition type of FAT32

2.  If you already had an EFI partition, mount it as /boot/efi (but **DO NOT** format it)

3.  If you want, make a 8GB SWAP partition (more info [here](../Linux/Components/partitions.md#swap))

4.  Create your root partition formatted as ext4 with how much space you want to give Linux ([more info about partitions](../Linux/Components/partitions.md)). See [here](./notes.md#how-much-space-does-linux-need) if you are unsure how much to give.

5.  If you want a separate /home (your files, see [here](../Linux/Components/folder-layout.md#home) for more information), which is recommended, create one now with Ext and the rest of the space.

6.  Hit Apply


## Alternate BIOS Keys

Rather than pressing F2, try the key below that matches the manufacturer of your laptop.

-   ASRock: F2 or DEL

-   ASUS: F2 for all PCs, F2 or DEL for Motherboards

-   Acer: F2 or DEL

-   Dell: F2 or F12

-   ECS: DEL

-   Gigabyte / Aorus: F2 or DEL

-   HP: F10

-   Lenovo (Consumer Laptops): F2 or Fn + F2

-   Lenovo (Desktops): F1

-   Lenovo (ThinkPads): Enter then F1.

-   MSI: DEL for motherboards and PCs

-   Microsoft Surface Tablets: Press and hold volume up button.

-   Origin PC: F2

-   Samsung: F2

-   Toshiba: F2

-   Zotac: DEL

[Source: Tom's Hardware](https://www.tomshardware.com/reviews/bios-keys-to-access-your-firmware,5732.html)

## Hard drive not detected

1.  Go into your BIOS (instructions [here](./Target/bare-metal.md#entering-the-bios))

2.  Look through settings relating to hard drives (sometimes under configuration)

3.  Turn off Optane

4.  If "SATA Controller Mode" is set to "RT" and you have an existing Windows install:

    1.  Save and Exit

    2.  Boot into Windows

    3.  Open Command prompt as Adminstrator

    4.  Execute `bcdedit /set {current} safeboot minimal`

    5.  Enter BIOS

    6.  Turn "SATA Control Mode" to "AHCI" (don't worry your data won't be lost)

    7.  Save and Exit

    8.  Boot into Windows

    9.  Open Command prompt as adminstrator

    10. Execute `bcdedit /deletevalue {current} safeboot`

5.  Save and exit if you are in the BIOS

6.  See if the hard drive is detected now


