# Pre-Installation
***IF YOU ARE EVER UNSURE OF HOW TO PROCEED, FEEL FREE TO [ASK](../LUG.md)!***

This section will occasionally reference alternate configurations, if you want that particular option, go to the associated section.

## Pick a Distro

Which [distro](../Linux/distros.md) you choose will be based on:

-   The speed of your computer
-   How stable you want it (normally older software is more stable)
-   The [Desktop Environment](../Linux/Components/desktop-environment.md)
-   And of course personal preference
See a brief list of distros [here](../Linux/distros.md)
## Select a Target Platform
### Bare Metal
This is installing directly onto your computer. Dual Booting instructions at the bottom.
### Virtualbox
This is for installing Linux in a [Virtual Machine](../Linux/Compartmentalization/Virtual-Machines/Virtualbox.md)
### Windows Subsystem for Linux
This is for those interested in being able to run a Linux terminal within Windows. This does not give you the full Linux experience.
